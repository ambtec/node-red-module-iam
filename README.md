# node-red-module-iam
Implementation for [Node-RED](https://nodered.org/) to connect with IAM Service (Identity Access Management) of ambtec.

## Installation
To install node-red-module-iam use this command

`npm i https://gitlab.com/ambtec/node-red-module-iam.git`

## Composition
The Socket.IO implementation is made with
* 1 login node
* 1 has group validation node
* 1 logout node

## Usage
To see an example just run node-red-service

## License
MIT
