import {
  NodeRedApp,
  EditorNodeProperties,
  EditorRED,
  NodeMessageInFlow,
} from "node-red";
import express, { Request, Response } from "express";

export interface NodeRedAppCustom extends NodeRedApp {
  nodes: any;
}

export interface ResponseCustom extends Response {
  data: ResponseCustomData;
}

export interface ResponseCustomData {
  client_id: string;
  refresh_token: string;
  refresh_expires_in: string;
  session_state: string;
  access_token: string;
}

export interface CustomEditorNodeProperties extends EditorNodeProperties {
  endpoint: string;
  path: string;
  group: string;
}
