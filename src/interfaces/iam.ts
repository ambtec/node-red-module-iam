interface IamBasics {
  grant_type: "authorization_code" | "refresh_token" | "password";
  client_id: "account" | "app-vue";
  scope: "openid";
}

export interface CustomResponse extends Response {
  data: any;
  on: any;
  statusCode: number;
}

export interface IamData {
  userId: string;
  client_id: string;
  token: string;
  tokenExpiresIn: string;
  session: string;
  resource_access: object;
  groups: string[];
  username: string;
}

export interface IamDefaultInputPayload extends IamBasics {
  token?: string | null;
  username?: string | null;
  password?: string | null;
  code?: string | null;
  redirect_uri?: string | null;
}

export interface IamConfig extends IamDefaultInputPayload {}

export interface InputPayloadIam extends IamDefaultInputPayload {}

export interface SendPayload extends IamBasics {
  client_secret?: string;
  code?: string;
  refresh_token?: string;
  redirect_uri?: string;
  username?: string;
  password?: string;
}

export interface URLSearchParamsPayload extends URLSearchParams, SendPayload {}

export interface ResponsePayload {
  statusCode: number;
  data: IamData | null;
}

export interface ResponseIamData {
  data: ResponseIamConfiguration;
}

export interface ResponseIamConfiguration {
  username: string;
  resource_access: string;
  groups: string[];
}

export interface ResponseCustomData {
  client_id: string;
  refresh_token: string;
  refresh_expires_in: string;
  session_state: string;
  access_token: string;
}

export interface InputDefault {
  iam: IamData;
  payload: InputPayloadIam;
  req: InputPayloadReq;
}

export interface InputPayloadReq {
  headers: InputPayloadReqHeaders;
}

export interface InputPayloadReqHeaders {
  authorization: string;
}

export interface SendError {
  data: string;
  statusCode: number;
  properties: object;
}
