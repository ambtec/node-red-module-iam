import {
  CustomResponse,
  IamData,
  SendPayload,
  URLSearchParamsPayload,
  ResponsePayload,
  ResponseIamConfiguration,
  ResponseIamData,
  SendError,
  ResponseCustomData,
} from "../interfaces/iam";

// @ts-ignore
import Log from "ambtec-logger";
import http from "http";

class Iam {
  constructor() {}

  send(endpoint: string, payload: SendPayload): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const [HOST, PORT] = process.env.IAM_SERVICE_HOST.split(":");
      const requestPayload = <URLSearchParamsPayload>(
        new URLSearchParams(
          <URLSearchParamsPayload>payload ? <URLSearchParamsPayload>payload : {}
        )
      );

      const error: SendError = {
        data: "",
        statusCode: -1,
        properties: {
          HOST: HOST,
          PORT: PORT ? PORT : 80,
        },
      };

      if (!HOST) {
        reject({
          ...error,
          ...{
            data: "Invalid http configuration HOST",
          },
        });
        return;
      }

      if (!endpoint) {
        reject({
          ...error,
          ...{
            data: "Invalid endpoint configuration",
          },
        });
        return;
      }

      const options = {
        hostname: HOST,
        port: PORT ? PORT : 80,
        path: endpoint,
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Content-Length": requestPayload.toString().length,
        },
      };

      const req = http.request(options, (res: http.IncomingMessage): void => {
        let responseData = "";

        res.on("data", (chunk): void => {
          responseData += chunk;
        });

        res.on("end", (): void => {
          if (res.statusCode > 299) {
            resolve({
              data: null,
              statusCode: res.statusCode,
            });
            return;
          }

          responseData = JSON.parse(
            responseData ? responseData.toString() : "{}"
          );

          resolve({
            data: responseData,
            statusCode: res.statusCode,
          });
        });

        res.on("error", (error: Error): void => {
          reject({
            ...error,
            ...{
              data: "HTTP error on response",
              statusCode: res.statusCode,
              properties: {
                error: error,
              },
            },
          });
        });
      });

      req.on("error", (error: Error): void => {
        reject({
          ...error,
          ...{
            data: "HTTP error on request",
            properties: {
              error: error,
            },
          },
        });
      });

      req.write(requestPayload.toString());
      req.end();
    });
  }

  prepareIamNodeBody(data: ResponseCustomData): IamData {
    const iam: IamData = {
      userId: "",
      client_id: data.client_id,
      token: data.refresh_token,
      tokenExpiresIn: data.refresh_expires_in,
      session: data.session_state,
      resource_access: {},
      groups: [],
      username: "",
    };
    if (data.access_token) {
      const base64String = data.access_token.split(".")[1];
      const decodedValue = JSON.parse(
        Buffer.from(base64String, "base64").toString("ascii")
      );
      iam.userId = decodedValue.sub;
      iam.resource_access = decodedValue.resource_access;
      iam.groups = decodedValue.groups;
      iam.username = decodedValue.preferred_username;
    }
    return iam;
  }

  logout(token: string, client_id: string): void {
    if (!token) {
      return null;
    }

    if (!client_id) {
      client_id = process.env.IAM_DEFAULT_CLIENT_ID
        ? process.env.IAM_DEFAULT_CLIENT_ID
        : "account";
    }

    const payload: SendPayload = {
      grant_type: "refresh_token",
      // @ts-ignore
      client_id: `${client_id}`,
      client_secret: "",
      refresh_token: `${token}`,
      scope: "openid",
    };

    const logoutAddress = process.env.IAM_ADDRESS_LOGOUT
      ? process.env.IAM_ADDRESS_LOGOUT
      : "/auth/realms/ambtec/protocol/openid-connect/logout";
    this.send(logoutAddress, payload)
      .then((res): void => {
        Log.debug({
          breadcrumbs: ["Iam", "logout"],
          message: "User logged out",
          properties: {
            httpStatusCode: res.statusCode,
            data: res.data,
          },
        });
      })
      .catch((error): void => {
        Log.error({
          breadcrumbs: ["Iam", "logout"],
          message: "Request error happen",
          properties: error,
        });
      });
  }

  async getIamConfiguration(
    token: string,
    client_id: string
  ): Promise<ResponseIamData | null> {
    if (!token) {
      return null;
    }

    if (!client_id) {
      client_id = process.env.IAM_DEFAULT_CLIENT_ID
        ? process.env.IAM_DEFAULT_CLIENT_ID
        : "account";
    }

    const payload: SendPayload = {
      grant_type: "refresh_token",
      // @ts-ignore
      client_id: `${client_id}`,
      client_secret: "",
      refresh_token: `${token}`,
      scope: "openid",
    };
    const tokenAddress = process.env.IAM_ADDRESS_TOKEN
      ? process.env.IAM_ADDRESS_TOKEN
      : "/auth/realms/ambtec/protocol/openid-connect/token";
    return this.send(tokenAddress, payload);
  }

  async keepAlive(token: string, client_id: string): Promise<any> {
    if (!token) {
      return;
    }

    if (!client_id) {
      client_id = process.env.IAM_DEFAULT_CLIENT_ID
        ? process.env.IAM_DEFAULT_CLIENT_ID
        : "account";
    }

    const payload: SendPayload = {
      grant_type: "refresh_token",
      // @ts-ignore
      client_id: `${client_id}`,
      client_secret: "",
      refresh_token: `${token}`,
      scope: "openid",
    };

    const tokenAddress = process.env.IAM_ADDRESS_TOKEN
      ? process.env.IAM_ADDRESS_TOKEN
      : "/auth/realms/ambtec/protocol/openid-connect/token";
    return await this.send(tokenAddress, payload).catch((error): void => {
      Log.error({
        breadcrumbs: ["Iam", "keepAlive"],
        message: "Request error happen",
        properties: error,
      });
    });
  }
}

export default Iam;
